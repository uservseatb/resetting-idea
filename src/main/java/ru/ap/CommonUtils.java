package ru.ap;


public class CommonUtils {

    public static IllegalStateException newIllegalStateException(String format, Object... args) {
        return new IllegalStateException(String.format(format, args));
    }

    public static void println(String format, String... args) {
        System.out.println(
                String.format(format, args)
        );
    }
}
