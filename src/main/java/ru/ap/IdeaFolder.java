package ru.ap;

import org.apache.commons.io.FileUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import static ru.ap.CommonUtils.newIllegalStateException;

public final class IdeaFolder {

    private static final String EVAL_PROPERTY_NAME_PREFIX = "evlsprt";
    private static final String EVAL_PROPERTY_NEW_VALUE = " value=\"x\" />";
    private static final String KEYS_FOLDER_NAME = "eval";
    private static final String OPTIONS_FILE_NAME = "options/options.xml";
    private static final Pattern KEY_FILE_NAME_PATTERN = Pattern.compile("idea[0-9]+\\.evaluation\\.key");
    private static final int EVAL_PROPERTY_NAME_TAG_INDEX = 20;

    private final String ideaFolderName;

    public IdeaFolder(String ideaFolderName) {
        this.ideaFolderName = ideaFolderName;
    }

    public void checkIfCorrect() {

        Path ideaFolderPath = Paths.get(ideaFolderName);
        Path keyFolderPath = keyFolderPath();

        boolean isIdeaFolderPathExists = Files.exists(ideaFolderPath);
        if (!isIdeaFolderPathExists) {
            throw newIllegalStateException("Path %s does not exists", ideaFolderName);
        }

        boolean isIdeaFolderPathDirectory = Files.isDirectory(ideaFolderPath);
        if (!isIdeaFolderPathDirectory) {
            throw newIllegalStateException("Path %s is not a folder", ideaFolderName);
        }

        boolean isTheKeyFolderExists = Files.exists(keyFolderPath);
        if (!isTheKeyFolderExists) {
            throw newIllegalStateException("Path %s is not idea folder", ideaFolderName);
        }
    }

    public void resetKeyFile() throws IOException {

        Path keyFilePath = fetchKeyFilePath();
        File keyFile = keyFilePath.toFile();
        FileUtils.forceDelete(keyFile);
        Files.createFile(keyFilePath);

        try (
            FileOutputStream fileOutputStream = new FileOutputStream(keyFile);
            DataOutputStream localDataOutputStream = new DataOutputStream(fileOutputStream)
        ) {
            localDataOutputStream
                .writeLong(~(System.currentTimeMillis()));
        }
    }

    public void resetOptionFile() throws IOException {
        File optionFile = fetchOptionFile();
        List<String> fileContent = FileUtils.readLines(optionFile, StandardCharsets.UTF_8);

        List<String> newContent = new ArrayList<>();
        for (String line : fileContent) {
            newContent.add(processOptionFileLine(line));
        }

        FileUtils.writeLines(optionFile, newContent);
    }

    private String processOptionFileLine(String line) {
        return line.contains(EVAL_PROPERTY_NAME_PREFIX) ?
            replaceValueOfEvalProperty(line) :
            line;
    }

    private String replaceValueOfEvalProperty(String evalPropertyLine) {
        int beforeValueIndex = evalPropertyLine.indexOf("\"", EVAL_PROPERTY_NAME_TAG_INDEX);
        return evalPropertyLine
            .substring(0, beforeValueIndex + 1)
            .concat(EVAL_PROPERTY_NEW_VALUE);
    }

    private Path fetchKeyFilePath() throws IOException {

        return Files
            .list(keyFolderPath())
            .filter(path -> KEY_FILE_NAME_PATTERN
                .matcher(path.getFileName().toString())
                .matches())
            .max(Comparator.reverseOrder())
            .orElseThrow(() -> newIllegalStateException("There is no key files in this %s folder", keyFolderPath()));
    }

    private File fetchOptionFile() {
        Path optionFilePath = Paths.get(ideaFolderName, OPTIONS_FILE_NAME);

        if (!Files.exists(optionFilePath)) {
            throw newIllegalStateException("The file %s is not found", optionFilePath);
        }

        return optionFilePath.toFile();
    }

    private Path keyFolderPath() {
        return Paths.get(ideaFolderName, KEYS_FOLDER_NAME);
    }
}
