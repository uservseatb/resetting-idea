package ru.ap;

import static ru.ap.CommonUtils.println;

public class Main {

    private static final int DEFAULT_PROLONGATION_PERIOD_IN_DAYS = 30;

    public static void main(String[] args) {

        try {
            run(new Params(args));
        } catch (IllegalStateException businessError) {
            println(businessError.getMessage());
        } catch (Exception unknown) {
            println("OOPs...");
            println(unknown.getMessage());
        }
    }

    private static void run(Params params) throws Exception {

        String ideaFolderName = params.getIdeaFolderPathName();

        IdeaFolder ideaFolder = new IdeaFolder(ideaFolderName);

        ideaFolder.checkIfCorrect();
        ideaFolder.resetKeyFile();
        ideaFolder.resetOptionFile();
    }
}


