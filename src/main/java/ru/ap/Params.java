package ru.ap;


public final class Params {

    public static final int IDEA_FOLDER_PATH_PARAM_INDEX = 0;

    private String[] args;

    public Params(String[] args) {
        checkArguments(args);
        this.args = args;
    }

    public String getIdeaFolderPathName() {
        return args[IDEA_FOLDER_PATH_PARAM_INDEX];
    }

    private void checkArguments(String[] args) {
        if (args.length == 0) {
            throw new IllegalStateException("You should pass idea folder path as the first argument");
        }
    }
}
